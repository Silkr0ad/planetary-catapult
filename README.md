# Planetary Catapult (temporary name)

A very simple mobile game. Literally, the main point of the project is to make it as simple as can be.

![Showcase GIF](Media/planetary_catapult.gif)

## Branches

### master

Recently updated the README, but is otherwise very behind in development.

### add_juice

The latest branch. Stuck on where to take the game in regards to mechanics, so I thought I should at least juice it in the meantime.

### rigidbody-projectile

Briefly flirted with using Godot's built-in physics system instead of my own. Was scrapped. Should probably delete the branch, actually.

### encourage_orbit

An even briefer attempt to count orbits made around the planet and use it as a base for some other mechanic(s). Might take it back up later, so it stays.


## Attributions

This area is for crediting external assets.
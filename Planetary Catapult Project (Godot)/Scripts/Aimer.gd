extends Node2D

var coordinates
var projectile
var color
#var max_vel

func _ready():
	projectile = get_node("../Projectile")
	color = Color("ffbd69")
	#max_vel = projectile.max_vel
	coordinates = [Vector2.ZERO, Vector2.ZERO]

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("aim"):
		# get starting position
		coordinates[0] = get_viewport().get_mouse_position()
	elif Input.is_action_pressed("aim"):
		# get current position
		coordinates[1] = get_viewport().get_mouse_position()
		var difference = coordinates[1] - coordinates[0]
#		if difference.length_squared() > pow(max_vel, 2):
#			coordinates[1] = coordinates[0] + difference.normalized() * projectile.max_vel
		update()
	elif Input.is_action_just_released("aim"):
		projectile.fire(coordinates)
		coordinates[0] = Vector2.ZERO
		coordinates[1] = Vector2.ZERO
		update()

func _draw():
	if coordinates.size() == 2:
		draw_line(coordinates[0], coordinates[1], color, 3.0)

extends Node2D

var coordinates
var projectile
var color
var max_vel

func _ready():
	projectile = get_node("../Projectile")
	color = Color("ffbd69")
	max_vel = projectile.max_vel

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_pressed("aim"):
		coordinates = get_viewport().get_mouse_position()
		var difference = coordinates - projectile.position
		if difference.length_squared() > pow(max_vel, 2):
			var direction = difference.normalized()
			coordinates = projectile.position + direction * projectile.max_vel
		update()
	elif Input.is_action_just_released("aim"):
		projectile.fire(coordinates)
		coordinates = Vector2.ZERO
		update()

func _draw():
	if coordinates != null and coordinates != Vector2.ZERO:
		draw_line(projectile.position, coordinates, color, 3.0)

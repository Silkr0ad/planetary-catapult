extends KinematicBody2D

export (Resource) var explosion

const G = 1000

signal fired()
signal hit(hit_pos)

var camera
var planet
var planet_radius
var current_vel = Vector2.ZERO
var attraction_limit = .5
var shot_elapsed = 0
var max_vel = 20
var fire_to_planet

var should_move = false

func _ready():
	camera = get_node("../Camera")
	planet = get_node("../Planet")
	get_radia()

func _physics_process(delta):
	if should_move:
		shot_elapsed += delta
		
		current_vel += get_gravity()
		var collision = move_and_collide(current_vel, false)
		rotation = current_vel.angle() + deg2rad(90)
		
		if (collision != null):
			handle_collision(collision)
		
		check_for_orbit()

func get_gravity():
	var vec = position - get_closest_point_on_planet()
	var fixed_attraction = 1 + shot_elapsed * 5
	return -vec.normalized() * fixed_attraction

func get_radia():
	if (planet.scale.x != planet.scale.y):
		print("Planet's x scale != its y scale.")
		return -1
	elif scale.x != scale.y:
		print ("Projectile's x scale != its y scale.")
		return -1
		
	planet_radius = planet.get_node("CollisionShape2D").shape.radius * planet.scale.x
	#self_radius = get_node("CollisionShape2D").shape.radius * scale.x

func get_closest_point_on_planet():
	var direction = (position - planet.position).normalized()
	return planet.position + direction * planet_radius

func fire(coordinates):
	current_vel = ((coordinates[1] - coordinates[0]) / 10).clamped(max_vel)
	position = coordinates[0]
	shot_elapsed = 0
	should_move = true
	fire_to_planet = (coordinates[0] - planet.position)
	emit_signal("fired")
	# update the trail
	update()

func handle_collision(collision):
	if collision.collider is StaticBody2D:
		emit_signal("hit", collision.position)
		var boom = explosion.instance()
		boom.set_name("Explosion")
		boom.position = position
		boom.rotation = (-(position - planet.position)).angle() - deg2rad(90)
		get_parent().add_child(boom)
		boom.play()
		current_vel = 0
		position = Vector2(-100, 0)
		camera.shake = true
	else:
		collision.collider.apply_impulse(Vector2.ZERO, current_vel * 100)
	
	should_move = false

func check_for_orbit():
	var proj_to_planet = (position - planet.position).normalized()
	if proj_to_planet.dot(fire_to_planet) == 1:
		print("ORBIT COMPLETED")

func _draw():
	draw_set_transform(-position + planet.position, 0, Vector2(1, 1))
	if fire_to_planet != null:
		#draw_line(Vector2.ZERO, fire_to_planet, Color.red)
		pass

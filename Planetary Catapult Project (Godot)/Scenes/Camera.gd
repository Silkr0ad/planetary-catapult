extends Camera2D

onready var timer : Timer

export var amplitude := 6.0
export var duration := 0.8 setget set_duration
export(float, EASE) var DAMP_EASING := 1.0
export var shake := false setget set_shake

var enabled := false

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	timer = get_node("ShakeTimer")
	randomize()
	#set_process(false)
	self.duration = duration

func _process(delta: float) -> void:
	var damping = ease(timer.time_left / timer.wait_time, DAMP_EASING)
	#print(damping)
	offset = Vector2(
		rand_range(amplitude, -amplitude) * damping,
		rand_range(amplitude, -amplitude) * damping
	)

func set_duration(value: float) -> void:
	duration = value
	if timer:
		timer.wait_time = duration

func set_shake(value: bool) -> void:
	shake = value
	set_process(true)
	offset = Vector2()
	if shake and timer:
		timer.start()
	elif !shake:
		timer.stop()


func _on_ShakeTimer_timeout() -> void:
	self.shake = false

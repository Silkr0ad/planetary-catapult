# Big thanks to John Watson, Graviy Aces's dev, for the tutorial.

extends Node2D

export var MAX_LENGTH: int = 200
export var THICKNESS = 3
export (NodePath) var target_path

var frame: int = 0
var fade_only: bool = false

var target: Node2D
var points = []

func _ready() -> void:
	target = get_node(target_path)
	position = Vector2(0,0)

# physics process is used because the projectile uses it
func _physics_process(delta: float) -> void:
	# update every nth frame...
	if frame % 1 == 0:
		# prepend the target's position & make sure the array's not 'full'
		if not fade_only:
			points.push_front(target.global_position)
		if points.size() > MAX_LENGTH or fade_only:
			points.pop_back()
		
	# increment the frame counter and update _draw()
	frame += 1
	update()

func _draw() -> void:
	# a line needs at least 2 points
	if (points.size() < 2):
		return
		
	# set up some necessary variables
	var antialias = false
	var c = modulate
	var s = float(points.size())
	var adjusted = PoolVector2Array()
	var colors = PoolColorArray()
	
	# create offset points and color ranges
	for i in range(s):
		adjusted.append(points[i] - global_position)
		c.a = lerp(1.0, 0.0, i/s)
		colors.append(c)
	
	# draw the line
	draw_set_transform(Vector2(0,0), -get_parent().rotation, Vector2(1,1))
	draw_polyline_colors(points, colors, THICKNESS, antialias)

func clear_trail():
	points.clear()


func _on_Projectile_fired() -> void:
	clear_trail()
	fade_only = false


func _on_Projectile_hit(hit_pos) -> void:
	fade_only = true
	points.push_front(hit_pos)

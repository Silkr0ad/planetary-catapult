extends Node2D

# This is the game manager.

func _input(event):
	if event is InputEvent:
		if event.is_action_pressed("restart"):
			restart()

func restart():
	get_tree().reload_current_scene()
